import React from "react";

function GuessInput({ gameState, guess, setGuess, addSubmittedGuess }) {
  return (
    <form
      className="guess-input-wrapper"
      onSubmit={(event) => {
        event.preventDefault();
        console.log(`Guess is ${guess}`);
        addSubmittedGuess(guess);
        setGuess("");
      }}
    >
      <label htmlFor="guess-input">Enter guess:</label>
      <input
        id="guess-input"
        type="text"
        value={guess}
        pattern="\w{5,5}"
        required
        disabled={gameState !== "in-progress"}
        onChange={(event) => {
          setGuess(event.target.value.toUpperCase().substring(0, 5));
        }}
      />
    </form>
  );
}

export default GuessInput;
