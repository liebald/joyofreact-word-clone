import React from "react";

function Key({ submittedGuesses, letter }) {
  let classModifier = "unused";
  for (let i = 0; i < submittedGuesses.length; i++) {
    for (let j = 0; j < submittedGuesses[i].length; j++) {
      if (submittedGuesses[i][j].letter === letter) {
        // TOODO: This doesn't account for the same letter with multiple possible statuses across guesses. Fix this.
        classModifier = submittedGuesses[i][j].status;
        break;
      }
    }
  }
  return <span className={`cell ${classModifier}`}>{letter}</span>;
}

function Keyboard({ submittedGuesses }) {
  let rows = ["QWERTYUIOP", "ASDFGHJKL", "ZXCVBNM"];
  return (
    <div className="keyboard">
      {rows.map((row) => {
        return (
          <div className="keyboard-row" key={row}>
            {row.split("").map((c) => {
              return (
                <Key submittedGuesses={submittedGuesses} letter={c} key={c} />
              );
            })}
          </div>
        );
      })}
    </div>
  );
}

export default Keyboard;
