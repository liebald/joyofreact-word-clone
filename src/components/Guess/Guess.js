import React from "react";
import { range } from "../../utils";

function Guess({ guess }) {
  return (
    <p className="guess">
      {range(5).map((idx) => {
        let className = "cell";
        if (guess) {
          className = className + " " + guess[idx].status;
        }
        return (
          <span className={className} key={idx}>
            {guess && guess[idx].letter}
          </span>
        );
      })}
    </p>
  );
}

export default Guess;
