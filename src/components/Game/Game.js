import React from "react";

import { sample } from "../../utils";
import { checkGuess } from "../../game-helpers";
import { WORDS } from "../../data";
import { NUM_OF_GUESSES_ALLOWED } from "../../constants";
import GuessInput from "../GuessInput";
import GuessResults from "../GuessResults";
import GameBanner from "../GameBanner";
import Keyboard from "../Keyboard";

// To make debugging easier, we'll log the solution in the console.
function Game() {
  const [answer, setAnswer] = React.useState(() => sample(WORDS));
  const [guess, setGuess] = React.useState("");
  const [submittedGuesses, setSubmittedGuesses] = React.useState([]);
  const [gameState, setGameState] = React.useState("in-progress");

  React.useEffect(() => {
    console.info({ answer });
  }, []);

  function addSubmittedGuess(guess) {
    if (submittedGuesses.length === NUM_OF_GUESSES_ALLOWED) {
      window.alert("Max number of guesses reached!");
      return;
    }

    const checkedGuess = checkGuess(guess, answer);
    const nextSubmittedGuesses = [...submittedGuesses, checkedGuess];
    setSubmittedGuesses(nextSubmittedGuesses);
    if (guess === answer) {
      setGameState("won");
    } else if (submittedGuesses.length === NUM_OF_GUESSES_ALLOWED - 1) {
      // We just submitted our last guess, and it wasn't correct --> We lose
      setGameState("lost");
    }
  }

  function restartGame() {
    const newAnswer = sample(WORDS);
    console.info({ newAnswer });
    setAnswer(newAnswer);
    setGuess("");
    setSubmittedGuesses([]);
    setGameState("in-progress");
  }

  return (
    <>
      <GuessResults submittedGuesses={submittedGuesses} />
      <Keyboard submittedGuesses={submittedGuesses} />
      <GuessInput
        gameState={gameState}
        guess={guess}
        setGuess={setGuess}
        addSubmittedGuess={addSubmittedGuess}
      />
      <GameBanner
        submittedGuesses={submittedGuesses}
        gameState={gameState}
        answer={answer}
        restartFunc={restartGame}
      />
    </>
  );
}

export default Game;
