import React from "react";
import Guess from "../Guess";
import { range } from "../../utils";

function GuessResults({ submittedGuesses }) {
  return (
    <div className="guess-results">
      {range(6).map((idx) => {
        return <Guess guess={submittedGuesses[idx]} key={idx} />;
      })}
    </div>
  );
}

export default GuessResults;
