import React from "react";

function GameBanner({ gameState, answer, submittedGuesses, restartFunc }) {
  if (gameState === "in-progress") {
    return <></>;
  } else if (gameState === "won") {
    return (
      <div className="happy banner">
        <p>
          <strong>Congratulations!</strong> Got it in
          <strong>{submittedGuesses.length} guesses</strong>
        </p>
        <button onClick={restartFunc}>Restart Game</button>
      </div>
    );
  } else if (gameState === "lost") {
    return (
      <div className="sad banner">
        <p>
          Sorry, the correct answer is <strong>{answer}</strong>.
        </p>
        <button onClick={restartFunc}>Restart Game</button>
      </div>
    );
  }
}

export default GameBanner;
